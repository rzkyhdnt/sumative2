use employeedb;
select * from departement;

select employee.dep_id Departemen_ID, departement.dep_name Nama_Departemen, year(hireDate) Tahun, count(employee_id) Jumlah_Karyawan
from employee
join departement on employee.dep_id = departement.dep_id
GROUP BY employee.dep_id;
