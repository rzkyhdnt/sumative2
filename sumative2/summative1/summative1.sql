create database summativedb;
show databases;
use summativedb;

CREATE table student(
id int not null auto_increment,
name varchar(20),
surname varchar(10), 
birthDate date,
gender varchar(10),
primary key(id));

CREATE table lesson(
id int not null auto_increment, 
name varchar(20),
level int,
primary key(id));

CREATE table score(
id int not null auto_increment,
student_id int,
lesson_id int, 
score int,
primary key(id),
CONSTRAINT pk_student_id foreign key (student_id) 
REFERENCES student(id),
CONSTRAINT pk_lesson_id foreign key (lesson_id)
REFERENCES lesson(id)
);

show tables;

INSERT INTO student(name, surname, birthDate, gender) VALUES
("Kroyo Sutoyo", "Oyo", "1997-03-21", "Laki-Laki"),
("Ferdinand Hutapea", "Ferdi", "1998-07-01", "Laki-Laki"),
("Sri Ningsih Hasibuan", "Sri", "1997-11-09", "Perempuan"),
("Dewi Purnamasari", "Dewi", "1997-05-26", "Perempuan"),
("Jeremy Paguyuban", "Emy", "1997-02-19", "Laki-Laki"),
("Stephanus Markotop", "Anus", "1998-08-18", "Laki-Laki");

INSERT INTO lesson(name, level) VALUES 
("Bahasa Indonesia", 7),
("Matematika", 9),
("Fisika", 8),
("Bahasa Inggris", 8),
("PKN", 6);

ALTER table score auto_increment = 1;

INSERT INTO score(student_id, lesson_id, score) VALUES 
(1, 1, 65), (2, 1, 70), (3, 1, 80), (4, 1, 80), (5, 1, 80), (6, 1, 77),
(1, 2, 80), (2, 2, 60), (3, 2, 55), (4, 2, 90), (5, 2, 82), (6, 2, 75),
(1, 3, 76), (2, 2, 90), (3, 3, 78), (4, 3, 85), (5, 3, 88), (6, 3, 80),
(1, 4, 80), (2, 2, 75), (3, 4, 80), (4, 4, 88), (5, 4, 83), (6, 4, 82),
(1, 5, 90), (2, 2, 92), (3, 5, 80), (4, 5, 94), (5, 5, 89), (6, 5, 89);

SELECT * FROM student, lesson, score;
