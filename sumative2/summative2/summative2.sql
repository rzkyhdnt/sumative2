SELECT student.name Nama, lesson.name Pelajaran, score.score Nilai
FROM score
JOIN student ON student.id = score.student_id
JOIN lesson ON lesson.id = score.lesson_id;
