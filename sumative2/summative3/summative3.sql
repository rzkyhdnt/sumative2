show databases;
create database employeedb;
use employeedb;

CREATE TABLE employee(
id int not null auto_increment,
employee_id int,
firstName text,
lastName text,
email text,
phoneNumber text,
hireDate date,
job_id text,
salary double,
commission double,
manager_id int,
dep_id int,
primary key(id));

LOAD DATA INFILE 'summative2.csv' INTO TABLE employee 
FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n';

select * from employee;