use employeedb;
show create table employee;
alter table employee add primary key (id);
alter table employee modify hireDate date;
select * from employee where month(hireDate) = 08;