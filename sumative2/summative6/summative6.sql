-- MySQL dump 10.13  Distrib 8.0.24, for Win64 (x86_64)
--
-- Host: localhost    Database: employeedb
-- ------------------------------------------------------
-- Server version	8.0.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `departement`
--

DROP TABLE IF EXISTS `departement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `departement` (
  `id` int NOT NULL,
  `dep_id` int DEFAULT NULL,
  `dep_name` text,
  `mgr_id` int DEFAULT NULL,
  `loc_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departement`
--

LOCK TABLES `departement` WRITE;
/*!40000 ALTER TABLE `departement` DISABLE KEYS */;
INSERT INTO `departement` VALUES (1,10,'Administration',200,1700),(2,20,'Marketing',201,1800),(3,30,'Purchasing',114,1700),(4,40,'Human Resources',203,2400),(5,50,'Shipping',121,1500),(6,60,'IT',103,1400),(7,70,'Public Relations',204,2700),(8,80,'Sales',145,2500),(9,90,'Executive',100,1700),(10,100,'Finance',108,1700),(11,110,'Accounting',205,1700),(12,120,'Treasury',0,1700),(13,130,'Corporate Tax',0,1700),(14,140,'Control And Credit',0,1700),(15,150,'Shareholder Services',0,1700),(16,160,'Benefits',0,1700),(17,170,'Manufacturing',0,1700),(18,180,'Construction',0,1700),(19,190,'Contracting',0,1700),(20,200,'Operations',0,1700),(21,210,'IT Support',0,1700),(22,220,'NOC',0,1700),(23,230,'IT Helpdesk',0,1700),(24,240,'Government Sales',0,1700),(25,250,'Retail Sales',0,1700),(26,260,'Recruiting',0,1700),(27,270,'Payroll',0,1700);
/*!40000 ALTER TABLE `departement` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-05 11:02:46
